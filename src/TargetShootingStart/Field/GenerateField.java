package TargetShootingStart.Field;

import TargetShootingStart.RandGen.Rand;

public class GenerateField {
    static int[][] field = new int[5][5];
    public static void genField(){
        for (int i = 0; i < field.length; i++) {// Створення ігрового поля
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = 0;
            }
        }
    }
    public static void outField(){

        for (int[] rowArray : field) {
            for (int value : rowArray) {// Виивід ігрового поля
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }

    public static void resField(int r,int c){//позначення попаданн.Та оновлення поля
       field[r - 1][c - 1] = 8;
    }
    public static void fightTarget(int r,int c){//згенерована ціль
        field[r][c] = 1;
    }public static void hitMark(int r,int c){//згенерована ціль
        field[r][c] = 3;
    } // позначає ціль


}
