package TargetShootingStart.Game;

import TargetShootingStart.Field.GenerateField;
import TargetShootingStart.RandGen.Rand;

import java.util.Scanner;

public class GameStart {

    public static void gameStart() {

        GenerateField.genField();//...

        Scanner scanner = new Scanner(System.in);

        System.out.println("All Set. Get ready to rumble!");
        GenerateField.fightTarget(Rand.genRov(),Rand.genCol());//Позначка попадання в ціль
        while (true) {
            // Рядок
            int row;
            do {
                try {
                    System.out.print("Введіть рядок для стрільби (1-5): ");
                    String input = scanner.nextLine();
                    row = Integer.parseInt(input);
                } catch (NumberFormatException e) {
                    System.out.println("Введіть число.");
                    row = -1;
                }
                if (row < 1 || row > 5) {
                    System.out.println("Невірне значення. Спробуйте ще раз.");
                }
            } while (row < 1 || row > 5 );

            // Стовпець
            int col;
            do {
                try {
                    System.out.print("Введіть стовпчик для стрільби (1-5): ");
                    String input = scanner.nextLine();
                    col = Integer.parseInt(input);

                } catch (NumberFormatException e) {
                    System.out.println("Введіть число.");
                    col = -1;
                }
                if (col < 1 || col > 5) {
                    System.out.println("Невірне значення. Спробуйте ще раз.");
                }
            } while (col < 1 || col > 5);

            GenerateField.resField(row,col);//...

            // Перевірка попадання
            if (row == Rand.rov + 1 && col == Rand.col + 1) {
                GenerateField.hitMark(Rand.rov,Rand.col);
                System.out.println("You have won!");
                break;
            }
            GenerateField.outField();
        }


       GenerateField.outField();// Вивід остаточного ігрового поля
        System.out.println("End game");

    }
}
